const source = require('../contracts/ENSDeployer.json');

export default class TopmonksRegistrar {
  constructor(config, networkId) {
    this.config = config;
    this.contract = new config.web3.eth.Contract(source.contracts['@ensdomains/ens-contracts/contracts/registry/FIFSRegistrar.sol'].FIFSRegistrar.abi, config.getRegistrarAddress(networkId));
  }

  async register(subdomain, address) {
    const node = this.config.web3.utils.sha3(subdomain);
    const account = this.config.web3.defaultAccount;
    return this.contract.methods.register(node, address)
      .send({
        from: account,
      });
  }

  debugValue() {
    return this.contract.events.DebugValue();
  }
}
