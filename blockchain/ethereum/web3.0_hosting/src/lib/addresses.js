function getENSAddress(networkId) {
  console.log('Regsitry Address for networkId : ' + networkId);
  return "0x94178bB8C2963d90c4C41Ee5eF889ed7d8BBb5a3"; // Address of ENS Registry Deployed on DLTESTNET
}
function getRegistrarAddress(networkId) {
  console.log('Registrar Address for networkId : ' + networkId);
  return "0x3B54105C96e159e297893D43452554886861d9d0"; // Addres of ENS Registrar Deployed on DLTESTNET
}
export default {
  getEnsAddress: (networkId) => getENSAddress(networkId),
  getRegistrarAddress: (networkId) => getRegistrarAddress(networkId),
}