var express = require('express');
const cors = require('cors');
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(__dirname + '/public'));

app.use(cors());
app.options('*', cors());


app.get('/health', function (req, res, next) {
    res.json({ status: 'UP' });
});
app.listen(8008);